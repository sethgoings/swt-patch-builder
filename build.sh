#!/bin/bash

#set -x
tag=v3738 #3.7.1 is v3738
buildid=3.7.1

cd $(dirname $0)

base=$(pwd)
build=$(pwd)/build
dist=$(pwd)/distrib
swt_dir=$(pwd)/swt-${tag}
swt_src_dir=${swt_dir}/eclipse.platform.swt/bundles/org.eclipse.swt
swt_bin_dir=${swt_dir}/eclipse.platform.swt.binaries/bundles
swt_win32_dir="${swt_src_dir}/Eclipse SWT/win32"
patch_dir=${base}/patches
bin_to_patch=$(pwd)/patches
win32_to_patches=$(pwd)/patches
swt_src_base_to_patches=$(pwd)/patches

windows_i386_src="${swt_bin_dir}/org.eclipse.swt.win32.win32.x86"
windows_x86_64_src="${swt_dir}/org.eclipse.swt.win32.win32.x86_64"
darwin_cocoa_src="${swt_dir}/org.eclipse.swt.cocoa.macosx"

windows_i386=${dist}/swt.windows.i386
windows_x86_64=${dist}/swt.windows.x86_64
linux_i386=${dist}/swt.linux.i386
linux_x86_64=${dist}/swt.linux.x86_64
darwin_carbon=${dist}/swt.darwin.carbon
darwin_cocoa=${dist}/swt.darwin.cocoa

mkdir -p ${build}
mkdir -p ${dist}
mkdir -p ${windows_i386}
mkdir -p ${windows_x86_64}
mkdir -p ${linux_i386}
mkdir -p ${linux_x86_64}
mkdir -p ${darwin_carbon}
mkdir -p ${darwin_cocoa}

function checkout() {
  mkdir ${swt_dir}
  pushd ${swt_dir}
  ( git clone git://git.eclipse.org/gitroot/platform/eclipse.platform.swt.git ) &
  ( git clone git://git.eclipse.org/gitroot/platform/eclipse.platform.swt.binaries.git ) &
  wait
  pushd eclipse.platform.swt
    git checkout ${tag}
  popd
  pushd eclipse.platform.swt.binaries
    git checkout ${tag}
  popd
  popd
}

function checkout-prebuilts() {
  mkdir -p ${build}
  pushd ${build}
  wget "http://mirror.cc.columbia.edu/pub/software/eclipse/eclipse/downloads/drops/R-3.7.1-201109091335/swt-3.7.1-gtk-linux-x86.zip"
  wget "http://mirror.cc.columbia.edu/pub/software/eclipse/eclipse/downloads/drops/R-3.7.1-201109091335/swt-3.7.1-gtk-linux-x86_64.zip"
  wget "http://mirror.cc.columbia.edu/pub/software/eclipse/eclipse/downloads/drops/R-3.7.1-201109091335/swt-3.7.1-carbon-macosx.zip"
  popd
}


function do_patch() {
pushd "${swt_src_dir}";
pushd "${swt_win32_dir}";
echo "####### Applying keypress patch #######";
patch -Np1 < ${win32_to_patches}/50020-swt37.patch;
echo "####### Applying AVG patch #######";
patch -Np1 < ${win32_to_patches}/Avg-identity-fix.patch;
echo "####### Applying icon patch #######";
popd;
patch -Np0 < ${swt_src_base_to_patches}/SWTPatch350751.patch;
popd;
}

function build() {
  build-windows-i386
  build-windows-x86_64
  build-cocoa
  build-linux
  build-carbon
}

function build-windows-i386() {
  pushd "${windows_i386_src}" &&
  ant -Dbuildid=${buildid} swtdownload &&
  cp swt-${buildid}-win32-win32-x86.zip swt-${buildid}-win32-win32-x86-patched.zip &&
  zip -u swt-${buildid}-win32-win32-x86-patched.zip "${bin_to_patch}/50020-swt37.patch" &&
  zip -u swt-${buildid}-win32-win32-x86-patched.zip "${bin_to_patch}/Avg-identity-fix.patch" &&
  mv swt-${buildid}-win32-win32-x86-patched.zip ${build} &&
  unzip "${build}/swt-${buildid}-win32-win32-x86-patched.zip" -d ${windows_i386} 
  popd  
}

function build-windows-x86_64() {
  pushd "${windows_x86_64_src}" &&
  ant -Dbuildid=${buildid} swtdownload &&
  cp swt-${buildid}-win32-win32-x86_64.zip swt-${buildid}-win32-win32-x86_64-patched.zip &&
  zip -u swt-${buildid}-win32-win32-x86_64-patched.zip "${bin_to_patch}/50020-swt37.patch" &&
  zip -u swt-${buildid}-win32-win32-x86_64-patched.zip "${bin_to_patch}/Avg-identity-fix.patch" &&
  mv swt-${buildid}-win32-win32-x86_64-patched.zip ${build} &&
  unzip "${build}/swt-${buildid}-win32-win32-x86_64-patched.zip" -d ${windows_x86_64} 
  popd
}

function build-cocoa() {
  pushd "${darwin_cocoa_src}" &&
  ant -Dbuildid=${buildid} swtdownload &&
  cp swt-${buildid}-cocoa-macosx.zip swt-${buildid}-cocoa-macosx-patched.zip &&
  zip -u swt-${buildid}-cocoa-macosx-patched.zip "${bin_to_patch}/SWTPatch350751.patch" &&
  mv swt-${buildid}-cocoa-macosx-patched.zip ${build} &&
  unzip "${build}/swt-${buildid}-cocoa-macosx-patched.zip" -d ${darwin_cocoa} 
  popd
}

function build-linux() {
  pushd ${build} &&
  unzip "swt-${buildid}-gtk-linux-x86.zip" -d "${linux_i386}" &&
  unzip "swt-${buildid}-gtk-linux-x86_64.zip" -d "${linux_x86_64}"
  popd
}

function build-carbon() {
  pushd ${build} &&
  unzip "swt-${buildid}-carbon-macosx.zip" -d "${darwin_carbon}"
  popd
}

function push() {
  ant -Dplatform=linux -Darch=x86_64 publish-ext-release-with -Divy.new.revision=${buildid} && 
  ant -Dplatform=linux -Darch=i386 publish-ext-release-with -Divy.new.revision=${buildid} &&
  ant -Dplatform=darwin -Darch=carbon publish-ext-release-with -Divy.new.revision=${buildid} &&
  ant -Dplatform=darwin -Darch=cocoa publish-ext-release-with -Divy.new.revision=${buildid} &&
  ant -Dplatform=windows -Darch=x86_64 publish-ext-release-with -Divy.new.revision=${buildid} &&
  ant -Dplatform=windows -Darch=i386 publish-ext-release-with -Divy.new.revision=${buildid} 
}

function clean() {
  ant clean
  rm -rf ${swt_dir}
}

function step_1() {
  clean &&
  checkout &&
  checkout-prebuilts
}

function step_2() {
  do_patch
}

function step_3() {
  build
  push
}

$1
# Requires 3 stages...
#step_1 
#step_2
#step_3
